use ggez::graphics::Color;
use ggez::nalgebra::{ Vector2 as Vec2 };
use rand::Rng;
use crate::bird::Bird;

pub struct Pipe
{
    pub position: Vec2<f32>,
    pub size: Vec2<f32>,
    pub color: Color,
    scored: bool,
}

impl Pipe
{
    pub fn new(color: Color, position: Vec2<f32>, size: Vec2<f32>, ) -> Pipe
    {
        Pipe
        {
            color: color,
            position: position,
            size: size,
            scored: false,
        }
    }
    pub fn intersects(&self, bird: &Bird) -> bool
    {
        if bird.position.x + bird.radius > self.position.x && bird.position.x - bird.radius < self.position.x + self.size.x
        {
            if self.position.y == 0_f32 && bird.position.y - bird.radius < self.size.y
            {
                return true
            }
            else if self.position.y > 0_f32 && bird.position.y + bird.radius > 600_f32 - self.size.y
            {
                return true
            }
        }
        false
    }

    pub fn scored(&mut self, bird: &Bird) -> bool
    {
        if self.position.x + self.size.x < bird.position.x - bird.radius && !self.scored
        {
            self.scored = true;
            return true;
        }
        false
    }

    pub fn update(&mut self)
    {
        self.position.x -= 5_f32;
        if self.position.x < -50_f32
        {
            self.scored = false;
            self.position.x = 1200_f32;
            let mut rand = rand::thread_rng();
            self.size.y = rand.gen::<f32>() * 100_f32 + 150_f32;

            if self.position.y != 0_f32
            {
                self.position.y = 600_f32 - self.size.y;
            }
        }
    }
}