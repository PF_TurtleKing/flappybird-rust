use ggez::graphics::Color;
use ggez::nalgebra::{ Point2 as P2 };

pub struct Bird
{
    pub position: P2<f32>,
    pub radius: f32,
    pub color: Color,
    velocity: f32,
    acceleration: f32,
}

impl Bird
{
    pub fn new(position: P2<f32>, radius: f32, color: Color) -> Bird
    {
        Bird
        {
            position: position,
            radius: radius,
            color: color,
            velocity: 0_f32,
            acceleration: 0_f32,
        }
    }

    pub fn hit_space_bar(&mut self)
    {
        self.acceleration -= 2_f32;
        self.velocity -= 2_f32;
        if self.acceleration < -1_f32
        {
            self.acceleration = -1_f32;
        }
    }

    pub fn update(&mut self)
    {
        self.position.y += self.velocity;
        self.acceleration += 0.08_f32;
        if self.acceleration > 1_f32
        {
            self.acceleration = 1_f32;
        }
        self.velocity += self.acceleration;
        if self.velocity > 5_f32
        {
            self.velocity = 5_f32;
        }
        if self.velocity < -5_f32
        {
            self.velocity = -5_f32;
        }
    }
}