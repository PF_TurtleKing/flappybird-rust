use ggez::ContextBuilder;
use ggez::event;

mod flappybird;
mod bird;
mod pipe;

use flappybird::FlappyBird;

fn main() {

    // Make a Context.
    let (mut ctx, mut event_loop) = ContextBuilder::new("my_game", "Cool Game Author")
        .window_setup(ggez::conf::WindowSetup::default().title("FlappyBird"))
        .window_mode(ggez::conf::WindowMode::default().dimensions(1200_f32, 600_f32))
		.build()
        .expect("aieee, could not create ggez context!");

    // Create an instance of your event handler.
    // Usually, you should provide it with the Context object to
    // use when setting your game up.
    let mut my_game = FlappyBird::new(&mut ctx);

    // Run!
    match event::run(&mut ctx, &mut event_loop, &mut my_game) {
        Ok(_) => println!("Exited cleanly."),
        Err(e) => println!("Error occured: {}", e)
    }
}