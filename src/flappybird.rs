use ggez::{graphics, Context, GameResult};
use ggez::event::EventHandler;
use ggez::nalgebra::{Point2 as P2, Vector2 as Vec2};
use rand::Rng;
use ggez::event::{KeyCode, KeyMods};

use crate::bird::Bird;
use crate::pipe::Pipe;

pub struct FlappyBird {
    bird: Bird,
    pipes: Vec<Pipe>,
    alive: bool,
    score: f64,
    high_score: f64,
}

impl FlappyBird {
    fn build_data() -> (Vec<Pipe>, Bird)
    {
        let bird = Bird::new
        (
            P2::new(150_f32, 150_f32),
            15_f32,
            graphics::Color::new(236_f32 / 255_f32, 245_f32 / 255_f32, 66_f32 / 255_f32, 255_f32)
        );

        let mut pipes = Vec::new();
        let mut rand = rand::thread_rng();

        for i in 0..4
        {
            let upper_pipe = Pipe::new(
                graphics::Color::new(105_f32 / 255_f32, 245_f32 / 255_f32, 66_f32 / 255_f32, 255_f32),
                Vec2::new(300_f32 + i as f32 * 300_f32, 0_f32),
                Vec2::new(50_f32, rand.gen::<f32>() * 100_f32 + 150_f32),
            );

            let size = Vec2::new(50_f32, rand.gen::<f32>() * 100_f32 + 150_f32);

            let lower_pipe = Pipe::new(
                graphics::Color::new(105_f32 / 255_f32, 245_f32 / 255_f32, 66_f32 / 255_f32, 255_f32),
                Vec2::new(300_f32 + i as f32 * 300_f32, 600_f32 - size.y),
                size,
            );

            pipes.push(upper_pipe);
            pipes.push(lower_pipe);
        }

        (pipes, bird)
    }

    fn reset(&mut self)
    {
        let data = FlappyBird::build_data();
        self.bird = data.1;
        self.pipes = data.0;
        self.alive = true;
        self.score = 0.0;
    }

    pub fn new(_ctx: &mut Context) -> FlappyBird {
        let data = FlappyBird::build_data();

        
        FlappyBird {
            bird: data.1,
            pipes: data.0,
            alive: true,
            score: 0.0,
            high_score: 0.0,
        }
    }
}

impl EventHandler for FlappyBird {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        if self.alive 
        {
            self.bird.update();
            for pipe in &mut self.pipes
            {
                pipe.update();
                if pipe.intersects(&self.bird)
                {
                    if self.score > self.high_score
                    {
                        self.high_score = self.score;
                    }
                    self.alive = false;
                }

                if pipe.scored(&self.bird)
                {
                    self.score += 3.1415 / 2.0;
                }
            }

            if self.bird.position.y < 0_f32 || self.bird.position.y > 600_f32
            {
                self.alive = false;
            }
        }
        
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx, graphics::Color::new(66_f32 / 255f32, 135_f32 / 255f32, 245_f32 / 255_f32, 1_f32));
        
        let bird = graphics::Mesh::new_circle(ctx, graphics::DrawMode::fill(), self.bird.position, self.bird.radius, 0.0001_f32, self.bird.color)?;
        
        for pipe in &self.pipes
        {
            let rect = graphics::Rect::new(pipe.position.x, pipe.position.y, pipe.size.x, pipe.size.y);
            let pipe = graphics::Mesh::new_rectangle(ctx, graphics::DrawMode::fill(), rect, pipe.color)?;
            graphics::draw(ctx, &pipe, (ggez::mint::Point2 { x: 0.0, y: 0.0 },))?;
        }
        
        graphics::draw(ctx, &bird, (ggez::mint::Point2 { x: 0.0, y: 0.0 },))?;

        let mut text = graphics::TextFragment::from( format!("Score: {:.4}", self.score) );
        text.color = Option::from(graphics::Color::new(0_f32, 0_f32, 0_f32, 1_f32));
        text.scale = Option::from(graphics::Scale{x: 30_f32, y: 30_f32});
        let text = graphics::Text::new(text);
        graphics::draw(ctx, &text, (ggez::mint::Point2 { x: 20.0, y: 20.0 },))?;

        if !self.alive
        {
            let mut text = graphics::TextFragment::from("Hart verkackt! ESC drücken ums noch mal zu probieren.");
            text.color = Option::from(graphics::Color::new(0_f32, 0_f32, 0_f32, 1_f32));
            text.scale = Option::from(graphics::Scale{x: 30_f32, y: 30_f32});
            let text = graphics::Text::new(text);
            graphics::draw(ctx, &text, (ggez::mint::Point2 { x: 250.0, y: 275.0 },))?;

            let mut text = graphics::TextFragment::from(format!("Current Highscore: {:.4}", self.high_score));
            text.color = Option::from(graphics::Color::new(0_f32, 0_f32, 0_f32, 1_f32));
            text.scale = Option::from(graphics::Scale{x: 30_f32, y: 30_f32});
            let text = graphics::Text::new(text);
            graphics::draw(ctx, &text, (ggez::mint::Point2 { x: 400.0, y: 300.0 },))?;
        }

		graphics::present(ctx)
    }

    fn key_down_event(&mut self, _ctx: &mut Context, keycode: KeyCode, _keymod: KeyMods, _repeat: bool)
    {
        if KeyCode::Space == keycode && self.alive
        {
            self.bird.hit_space_bar();
        }

        if KeyCode::Escape == keycode && !self.alive
        {
            self.reset();
        }
    }
}